/*
Navicat MySQL Data Transfer

Source Server         : 王睿本地MYSQL
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : whjava

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-08-09 11:48:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `creditcard` varchar(18) DEFAULT NULL,
  `photourl` varchar(50) DEFAULT NULL,
  `giturl` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('1', '李建鹏', '1', null, '420xxx19960724xx1x', null, 'https://git.oschina.net/ls_luo');
INSERT INTO `student` VALUES ('2', '何庆磊', '1', null, '421xxx19960603xx4x', null, 'https://git.oschina.net/Onama');
INSERT INTO `student` VALUES ('3', '陈庚炜', '1', null, '421xxx19970102xx1x', null, 'https://gitee.com/AGeng2');
INSERT INTO `student` VALUES ('4', '蔡江涛', '1', null, '421xxx19941025xx3x', null, 'https://gitee.com/JiangCai');
INSERT INTO `student` VALUES ('5', '黄昌浩', '1', null, '421xxx19941107xx3x', null, 'http://git.oschina.net/EnHengAction ');
INSERT INTO `student` VALUES ('6', '吴克兢', '1', null, '420xxx19960606xx1x', null, 'http://git.oschina.net/sorezzz ');
INSERT INTO `student` VALUES ('7', '刘莎', '1', null, '420xxx19930915xx4x', null, 'http://git.oschina.net/blacKill');
INSERT INTO `student` VALUES ('8', '冯凯', '1', null, '420xxx19940603xx3x', null, 'http://git.oschina.net/lfkail');
INSERT INTO `student` VALUES ('9', '孙畅', '1', null, '421xxx19970213xx2x', null, 'http://git.oschina.net/cochang');
INSERT INTO `student` VALUES ('10', '南航', '1', null, '421xxx19940911xx5x', null, 'https://gitee.com/itZhouJieLun');
INSERT INTO `student` VALUES ('11', '丁云', '1', null, '420xxx19950913xx2x', null, 'http://git.oschina.net/MaLiMaLi');
INSERT INTO `student` VALUES ('12', '蔡振东', '1', null, '421xxx19961105xx5x', null, 'https://gitee.com/DGAiShangDXG');
INSERT INTO `student` VALUES ('13', '居锦佺', '1', null, '421xxx19911008xx1x', null, 'https://git.oschina.net/java511520');
INSERT INTO `student` VALUES ('14', '夏梦莹', '1', null, '420xxx19950409xx2x', null, 'https://git.oschina.net/XiaXiaoRao');
INSERT INTO `student` VALUES ('15', '张硕', '1', null, '130xxx19950709xx1x', null, 'https://git.oschina.net/XiangWoZheYangDeRen');
INSERT INTO `student` VALUES ('16', '柳鑫', '1', null, '421xxx19950804xx3x', null, 'https://git.oschina.net/Rain1995 ');
