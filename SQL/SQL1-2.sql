-- 一.表结构
-- 1.增表
CREATE TABLE
IF NOT EXISTS `test` (
	id INT NOT NULL auto_increment,
	test VARCHAR (10),
	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;
-- 3.改表
alter table class add classname varchar(50);


-- engine是引擎，有innodb和myisam。innodb支持事务操作，myisam不支持事务操作。
-- 二.删除数据
-- 1.drop 删除物理表：drop table test
-- 2.delete 删除可视数据：delete from test [where ]
-- 3.TRUNCATE 删除所有信息（包括可视数据和主键数据）：truncate table test;
-- 三.新增数据
-- 1.基本新增
INSERT INTO test
VALUES
	(3, 'ddd');

-- 这种写法缺点： 既然已经设置了主键自增就不要再额外插入数据；如果这个sql写在java中，如果对此表的字段进行增删改，就会报错。
-- 标准写法应该如下：
INSERT INTO test (test, test2)
VALUES
	('aaa1', 'abc');


-- 2.查询新增
insert into login(username,`password`,isstudent) 
SELECT a.`name`,'123456',1
from student a;

-- 四.修改数据 UPDATE
-- 1.基本修改


UPDATE test -- 表名
SET test = 'bbb1'
WHERE
	id = 2;


-- 2.查询修改
UPDATE student -- 表名
SET classid = -- 1 -- 不知道具体是哪个班，只知道授课老师是王睿
(
SELECT a.id
from class a,teacher b 
where  1=1 
and a.teacherid=b.id
and b.`name`='王睿' 
-- and a.classname like '%武汉%'    
limit 0,1

)

;


-- 五.查询数据 （SQL的DDL（CRUD）命令中最重要的部分）
-- 1.基本查询
SELECT t.id,t.classid,t.`name`,t.telephone,t.photourl
from student t
;
SELECT a.id,a.address,a.classname
from class a,teacher b -- from 可以跟多张表，用逗号连接，表后可以跟alias(别名)。select的字段后可以跟alias(别名)
where  1=1 -- 如果from后有多张表，必须使用where限定表之间的关联关系，否则会产生笛卡儿积。
-- where习惯写1=1，就是占位。 SELECT...from ... where 1=1   + and ....
and a.teacherid=b.id
and b.`name`='王睿' -- sql里双引号不是官方字符。精确匹配使用=
and a.classname like '%武汉%'    -- 模糊匹配。使用like+%
;
-- 2.嵌套查询
-- 基本查询的语法是select ... from ... where ... 那么嵌套查询的写法就是可以将基本查询中的任意一个...再换成一个基本查询。
-- 嵌套查询中别名绝对不能相同，即使是多次查询一张表，都不能别名相同。

-- 查询的顺序:select ... from ... where ...  先from,后where,最后select

-- 1. selct...嵌套
-- 查询学生表里的学生，他们的授课分别是谁
SELECT a.id,a.`name`,
(SELECT c.`name`
from class b,teacher c
where b.teacherid=c.id
limit 0,1) teachername
from student a
;
-- group BY teachername -- 如果你在用select时，用了别名，请不要再group by之后用这个别名，因为它不认识

-- 2.from...嵌套 （最常用嵌套，效率最高）相当于新建了视图View


SELECT
	a.id,
	a.`name`,
 e.`name`
FROM
	student a,
	(
		SELECT
			c.`name`,
			b.id
		FROM
			class b,
			teacher c
		WHERE
			b.teacherid = c.id
		LIMIT 0,
		1
	) e
where a.classid=e.id

-- 3.where... 嵌套查询 
-- 查找学生表里不是南邮课程的同学
SELECT a.id,a.`name`
from student a
where 1=1
and a.classid not in   -- where中的嵌套一般要和not in \in .not exists \exists
(SELECT b.id 
 from class b 
 where a.classid=b.id and b.classname like '%南邮%')
;

SELECT a.id,a.`name`
from student a
where 1=1
and  not EXISTS  -- where中的嵌套一般要和not in \in .not exists \exists
(SELECT b.id 
 from class b 
 where a.classid=b.id and b.classname like '%南邮%')
;

-- 1.创建一个数据库studydb,要求支持简体中文

-- 2.创建一张表student,要求有序号，姓名，班级，手机，身份证号，照片地址
CREATE TABLE
IF NOT EXISTS `student` (
	id INT NOT NULL auto_increment,
	`name` VARCHAR (10),
classid int,
telephone varchar(15),
creditcard varchar(18),
photourl varchar(50),
	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;

-- 3.创建一张表teacher,要求有序号，姓名，手机，身份证号，照片地址
CREATE TABLE
IF NOT EXISTS `teacher` (
	id INT NOT NULL auto_increment,
	`name` VARCHAR (10),

telephone varchar(15),
creditcard varchar(18),
photourl varchar(50),
	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;
-- 4.创建一张表class,要求有序号，老师序号，地址，开班日期，结班日期，班主任的老师序号
CREATE TABLE
IF NOT EXISTS `class` (
	id INT NOT NULL auto_increment,
teacherid int,
	`address` VARCHAR (10),
startdate datetime,
endtime datetime,
masterid int,
	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;
-- 5.创建一张表exam,要求有序号，题目编号，题目，答案。
CREATE TABLE
IF NOT EXISTS `exam` (
	id INT NOT NULL auto_increment,
questionid int,
question text,
	answer text,

	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;
-- 6.创建一张表examresult,要求有序号，试题序号，学生序号，题目编号，学生答案。
CREATE TABLE
IF NOT EXISTS `examresult` (
	id INT NOT NULL auto_increment,
examid int,
questionid int,
studentid int,
	stuanswer text,

	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;

-- 7.创建一张表login,要求有序号，账号名称，账号密码，是否学生
CREATE TABLE
IF NOT EXISTS `login` (
	id INT NOT NULL auto_increment,
username varchar(50),
`password` varchar(50),
isstudent int,


	PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET = UTF8;

