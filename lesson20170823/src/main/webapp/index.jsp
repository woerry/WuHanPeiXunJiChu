<%@ page import="java.util.List" %>
<%@ page import="com.isoftstone.model.entity.User" %>
<%@ page import="com.isoftstone.model.dao.UserDao" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/8/22
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    session.setAttribute("cout","567");
%>
<h1>一:c:out标签</h1>
直接打印------><c:out value="123"></c:out><br>
配合EL表达式1------><c:out value="${sessionScope.cout}"></c:out><br>
配合EL表达式2------><c:out value="${1+3}"></c:out><br>
配合EL表达式,缺省值1------><c:out value="${sessionScope.cout1}">2333</c:out><br>
配合EL表达式,缺省值2------><c:out value="${sessionScope.cout1}" default="2334"></c:out><br>

<h1>二:c:set和c:move</h1>
<c:set value="wr" var="uid" ></c:set><br>
uid------>${uid}==${pageScope.uid}<br>
<c:remove var="userId"/> <br>
userId:--->${userId } <br>
<c:set value="${100}" var="wrval" ></c:set><br>
<h1>三.判断c:if</h1>
<c:if test="${wrval>90}">
    wrval的值大于90.<br>
</c:if>
<h1>四.判断c:choose,c:when,c:otherwise</h1>
<c:choose>
    <c:when test="${wrval>99}">
        wrval大于99.<br>
    </c:when>
    <c:otherwise>
        wrval小于99<br>
    </c:otherwise>

</c:choose>
<h1>五.循环c:foreach</h1>
<c:forEach var="i" begin="1" end="5" step="1">
    输出:<c:out value="${i}"></c:out><br>
</c:forEach>
<%
    UserDao ud=new UserDao();
    List<User> l=ud.getUserList();
    request.setAttribute("userlist",l);
%>

<table border="1px">
    <tr>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
    </tr>
    <c:forEach items="${userlist}" var="user" >
    <tr>
        <td><c:out value="${user.name}"></c:out></td>
        <td><c:out value="${user.sex}"></c:out></td>
        <td><c:out value="${user.age}"></c:out></td>
    </tr>
    </c:forEach>
</table>
</body>
</html>
