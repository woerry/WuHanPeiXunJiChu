package com.isoftstone.model.dao;

import com.isoftstone.model.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserDao {

    public List<User> getUserList(){
        List<User> l=new ArrayList<>();
        User u1=new User();
        u1.setName("小王");
        u1.setSex("男");
        u1.setAge(18);
        User u2=new User();
        u2.setName("小李");
        u2.setSex("女");
        u2.setAge(21);
        User u3=new User();
        u3.setName("小赵");
        u3.setSex("男");
        u3.setAge(30);
        l.add(u1);
        l.add(u2);
        l.add(u3);
        return l;
    }
}
