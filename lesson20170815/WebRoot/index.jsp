<%@ page language="java" import="java.util.*,com.isoftstone.project.dao.*" pageEncoding="UTF-8"%>
<%
//网页项目的四种作用域：
//request：请求：浏览器向服务器发送的请求
//,response：响应：服务器向浏览器发送的响应
//,session：会话：整个项目的会话
//,application：应用：整个中间件的应用

String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
    This is my JSP page. <br>
    <% 
    //<%中间的部门就是java代码。
    UserDao ud=new UserDao();
String name=ud.getUser().getName();
    %>
    <input type="text" value="<%=name %>" >
    <table style="border:1px">
    <%
    		for(int i=0;i<5;i++){
    			String n1=name;//这一行是为了获取记录集中的一行数据
    			%>
    			<tr><td><%=n1 %></td></tr>
    			<%
    		}
    %>
    </table>
  </body>
</html>
