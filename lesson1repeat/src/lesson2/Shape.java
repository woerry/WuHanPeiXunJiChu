package lesson2;

public abstract class Shape {//定义了抽象方法的类一定是抽象类
	{
		System.out.println("执行初始化块");
	}
	
	private String color;
	//抽象方法：计算周长
	public abstract Double calperimeter();
	//抽象方法：返回形状
	public abstract String getType();
	
	//抽象类的构造函数不是用来被实例化的，而是继承时被子类调用的。
	public Shape(){
		
	}
	
	public Shape(String color){
		System.out.println("执行shape构造器");
		this.color=color;
	}
	
	public void test(){
		
	}
	//1.抽象类必须用abstract修饰。
	//2.有抽象方法的类一定是抽象类。
	//3.抽象类不能被new实例化。
	//4.抽象类也可以有类的五元素。
}
