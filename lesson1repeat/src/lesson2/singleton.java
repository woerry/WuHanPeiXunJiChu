package lesson2;

public class singleton {
   //23种设计模式之首：单例模式
	//为数不多的，在面试中碰到的需要手写出来的面试题
	//作用：内存中实例化一个对象，并且让该类有且只有这么一个对象。
	//1.新建一个本类的静态变量
	private static singleton instance;
	//2.隐藏构造方法
	private singleton(){
		
	}
	//3.给一个实例化的方法。
	public static singleton getInstance(){
		//如果类instance没有被新建过，新建一次。
		if(instance==null){
			instance=new singleton();
		}
		return instance;
	}
	
	
}
