package lesson2;

import java.util.List;
import java.util.Map;

//java是单继承语言。类的单继承，接口是可以多继承
public interface IDog {
    int a=3;//常量
    public void getA();//方法不能有，只能是抽象方法、方法名
//    public IDog(){ //不存在构造函数
//    	
//    }
//    { //不能有块
//    	
//    }
    public class test{//可以有内部类
    	
    }
}
