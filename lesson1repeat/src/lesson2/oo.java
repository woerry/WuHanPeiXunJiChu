package lesson2;

import org.junit.Test;

public class oo {
	//类的构成：成员变量，方法，构造函数，块，内部类
	private String name;//field:成员变量/字段。 成员变量分为：
	//实例变量（供对象使用），类(静态)变量（供内使用的；static修饰符）
	//property:属性。带有getter/setter方法的成员变量。
	//如果一个类中的所有成员变量都是属性，那么这个类成为标准bean，javabean。
	//pojo简单java对象
	
	private String field="外部类的成员变量";
	
   public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	//构造函数又叫构造方法
	//1.能使用的修饰符只有作用域
	//2.构造方法没有返回值。不是void(没有值)
	//3.构造方法的名字必须与类名完全相同。
	

   
    //块。静态块和非静态块。
    //静态就是static修饰符修饰的，静态=类本身的。不需要实例化就可以直接调用
    static{
    	
    }
   //块可以写多个。是按照顺序执行。
    {
    	
    }
    //块是个伪概念。块在执行的时候是不存在，执行时它会消失，并且加载到无参构造函数中的最前端
    
    //内部类
    public class inclass{
    	private String field="内部类的成员变量";
    	public void info(){
    		String field="局部变量";
    		//内部类中访问外部内-》类名.this.变量名
    		System.out.println("外部类的成员变量："+oo.this.field);
    		System.out.println("内部类的成员变量："+this.field);
    		System.out.println("局部变量："+field);
    	}
    }
    
    @Test
public void goucheng(){
	   //面向过程：面条：买面粉，和面，擀面，浇头，煮面，端面，吃面。
	   //找专业面店（制面），买面，浇头，煮面，端面，吃面。
	   //面向对象（找专业的人做专业的事），优点：重复调用
	   //三大特性：
	   //1.封装（1.私密性 2.复用）
	   //private(私房钱):自己用的。 
       //public（公开支出）：所有人都能用。
       //protected（家族支出）：本人，子类
       //(default)（区域支出）：本人，同一个package邻居类
	   //2.继承（子类继承父类的成员变量和方法。唯独不能继承private）
	   //3.多态（重载不是多态，重写才是多态）
	   //同一件事，做第一次，面向过程和面向对象
    	//构造函数在调用的时候，其实先调用父类的构造函数，然后再调用本身。
	   inclass ic=new inclass();
	   ic.info();
	   System.out.println(ic.field);
	  
   }
    @Test
    public void playdog(){
    	 Dog dog=new Dog();
    	 Dog dog1=new Dog();
    	 System.out.println(dog==dog1);
  	   dog.setName("中华田园犬");
  	   dog.setColor("黄色");
  	   dog.call();
    }
    @Test
    public void playdog1(){
    	jiwawa jw=new jiwawa();
  	  jw.call();
    }
    
    @Test
    public void single(){
    	singleton st1=singleton.getInstance();
    	singleton st2=singleton.getInstance();
    	System.out.println(st1==st2);
    }
    //final最终的。
    //final修饰
    public final void finaltest(){
    	//1.final修饰变量。只能赋值一次。
    	final int a=3;//final修饰且赋值的，就是常量。
    	final int v;
    	System.out.println("a="+3);
    	//final修饰的类，就是丁克。
    }
    
    @Test
    public void abstractclass(){
    	//1.抽象类的构造方法
//    	Shape sp=new Shape();不能直接实例化
    	Shape s1=new triangle("蓝色", 3, 4, 5);
    	Shape s2=new Circle("红色", 4);
    	System.out.println(s1.getType());
    	System.out.println(s1.calperimeter());
    	System.out.println(s2.getType());
    	System.out.println(s2.calperimeter());
    	//更好的复用。
    }
    
    @Test
    public void interfaceclass(){
    	Hashiqi hsq=new Hashiqi();
    	IDog hsq1=new Hashiqi();
    	hsq1.getA();
    }
}
