package lesson2;

public class triangle extends Shape {//继承抽象类之后，必须实现全部的抽象方法
   private double a;
   private double b;
   private double c;
	
   public void sides(double a,double b,double c){
	   if(a>=b+c||b>=a+c||c>=a+b){
		   System.out.println("三角形必须是两边之和大于第三边");
		   return; //方法中的return会跳出整个方法。return后面可以带值。
	   }
	   this.a=a;
	   this.b=b;
	   this.c=c;
   }
   
   public triangle(String color,double a,double b,double c){
	   super(color);//子类中调用父类时就用super。Shape(color)
	   this.sides(a, b, c);
   }
   
	@Override
	public Double calperimeter() {
		// TODO Auto-generated method stub
		return a+b+c;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "三角形";
	}

}
