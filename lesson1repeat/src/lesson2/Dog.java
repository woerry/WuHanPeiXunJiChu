package lesson2;

public class Dog {
    private String name;
    protected String color;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
    public Dog(){
    	System.out.println("一只狗正在向你跑来");
    }
	public void call(){
		System.out.println(this.color+"的"+this.name+"在大叫");
	}
}
