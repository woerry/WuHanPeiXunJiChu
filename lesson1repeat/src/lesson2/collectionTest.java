package lesson2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import org.junit.Test;

public class collectionTest {
//List.
	@Test
	public void listtest(){
		Collection cl=null;//集合。collectin认为数组的面向对象形式。
		int a=3;//int 是基本类型
		int[] arr=new int[]{1,2,3};//int[]是引用类型
		//list
		List l=new ArrayList();
		l.add(1);//新增元素，元素会直接加到原list的最后
		l.add(2);
		l.add(4);
		l.add(3);
		l.add(5);
		l.add(6);
		l.add(7);
		l.add(6);
		System.out.println(l);
//		System.out.println(l.contains(2));//检查list，看是否有该元素
		l.remove(2);//删除元素，根据索引(从0开始计算)
//		System.out.println(l);
	}
	
	@Test
	public void settest(){
		Set<Integer> l=new HashSet<Integer>();
//		l.add(1);//新增元素，元素会直接加到原list的最后
//		l.add(2);
//		l.add(4);
//		l.add(3);
//		l.add(5);
//		l.add(6);
//		l.add(7);
//		l.add(6);
		for(int i=0;i<50;i++){
			l.add(i);
		}
		SortedSet ts=new TreeSet();
		for(int i=50;i>0;i--){
			ts.add(i);
		}
		
		System.out.println(ts);
//		for(Integer i:l){
//			System.out.print(i+"");
//		}
		//list数值可以重复，不自动排序。set值不能重复，自动排序。
	}
	
	@Test
	public void maptest(){
		Map<String,Object> m=new HashMap<String,Object>();
		//map存值是K/V
		m.put("name", "小黑");
		m.put("length", 6);
		m.put("name", "小白");
		System.out.println(m);
		System.out.println(m.get("name"));
		List<Map<String,Object>> l=new ArrayList<Map<String,Object>>();
		l.add(m);
		System.out.println(l);
		
	}
	
	//queue队列：queue是接口
	@Test
	public void queustest(){
//		Vector v;
		//队列：先进先出。FIFO。无底。
		Queue qu=new ArrayDeque();
		ArrayDeque qu1=new ArrayDeque();
		//ArrayDeque是queue的实现类
		//。双端队列，既可以模仿队列，又可以模仿栈。
		for(int i=1;i<10;i++){
			qu1.offer(i);//数组插入也可以offer，offer防止溢出。
		}
		System.out.println(qu1);
		qu.peek();
		qu.poll();
		System.out.println(qu1.peek());//获取第一个元素（第一个进队列）
		System.out.println(qu1.poll());//删除第一个元素（第一个出队列）
		System.out.println(qu1);//删除第一个元素（第一个出队列）
		
	}
	
	@Test
	public void queustest1(){
//		Vector v;
		//栈：后进先出。_IFO。有底。
		Queue qu=new ArrayDeque();
		ArrayDeque qu1=new ArrayDeque();
		//ArrayDeque是queue的实现类
		//。双端队列，既可以模仿队列，又可以模仿栈。
		for(int i=1;i<10;i++){
			qu1.push(i);//数组插入也可以offer，offer防止溢出。
			
		}
		System.out.println(qu1);
		
		System.out.println(qu1.peek());//获取第一个元素（第一个进队列）
		System.out.println(qu1.poll());//删除第一个元素（第一个出队列）
		System.out.println(qu1);//删除第一个元素（第一个出队列）
		
	}
	
	
	
}
