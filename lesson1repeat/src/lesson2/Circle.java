package lesson2;

public class Circle extends Shape {

	private double radius;
	
	public Circle(String color,double radius){
		super(color);
		this.radius=radius;
	}
	
	@Override
	public Double calperimeter() {
		// TODO Auto-generated method stub
		return 2*Math.PI*radius;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "圆形";
	}

}
