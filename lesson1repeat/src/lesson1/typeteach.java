package lesson1;

import org.junit.Test;

public class typeteach {
	
	//1.定义一个整数，初始化为23亿。2300000000
	public void definelong(){
		
		long a=2300000000L;
		//int是32位，2^32=21亿多。如果是整数型，=右边的值是java默认
		//整数型。java默认整数型是int。
		//java默认浮点型是double。
	}
	
	//2.将整数20转换为二进制，8进制，10进制，16进制。
	@Test
	public void OridixxxCast(){
		//jdk7之前不准许用户操作二进制。
		//20转换为二进制。正除反读
		byte a=(byte) 0B10010100;//二进制的数据，开头必须用0B表示。
		byte b=(byte) 0B11101100;
		//0B00010100求反：0B11101011；+1:0B11101100
		//Oct 八进制标志，Hex 16进制标志，Dec（decimal） 十进制 ，Bin 二进制
		//二进制转换到10进制。
		//10100->10进制  阶乘的数是该数值的第几位-1：
		//1*2^4+0*2^3+1*2^2+0*2^1+0*2^0=16+4=20
		System.out.println(a+":"+b);
		//IBM面试题：为什么程序员分不清万圣节和圣诞节。
		//
	}
	
	//3.char
	@Test
	public void testchar(){
		char zhong='软';
		int zhongChar=zhong;//char可以int互相转换的。早期是因为ASCII表，后来是因为UNICODE。
		System.out.println(zhongChar);
		short char1=(short) 65535;
		char char1fromint= (char) char1;
		System.out.println("转换="+(char)36719);
		
		//请找出“软通培训班”的char字符的unicode位置。
	}
	
	

}
