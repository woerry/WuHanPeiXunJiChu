package total;

import java.util.Scanner;

public class PageMenu {
 public PageMenu(){
	 System.out.println("-----------------");
	 System.out.println(" 请输入目录之前的序号");
	 System.out.println("-----------------");
	 System.out.println("-      目录                -");
	 System.out.println("- 1.JAVA初级教程      -");
	 System.out.println("- 2.JAVA中级教程      -");
	 System.out.println("- 3.JAVA高级教程      -");
	 System.out.println("- 4.退出系统                -");
	 System.out.println("-----------------");
	 
 }
 
 public void start(){
	 System.out.println("请输入序号：");
   	Scanner sc=new Scanner(System.in);
   	String id=sc.next();
   	switch(id){
   	case "1":new PageJunior();
   	case "2":new PageMiddle();
   	case "3":new PageSenior();
   	default :new PageLogin().start();
   	
   	}
	 
 }
}
