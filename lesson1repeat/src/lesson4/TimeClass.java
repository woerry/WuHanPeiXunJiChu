package lesson4;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class TimeClass {

	Date d=new Date();//d是有默认值的，就是运行这句话的时候得到的时间，精确到秒。
	
	@Test
	public void getToday(){
		//打印这个d
		System.out.println(d);
	}
	
	@Test
	public void getWeekday(){
		//日历类
		Calendar c=Calendar.getInstance();
		c.setTime(d);
		int weekday=c.get(Calendar.DAY_OF_WEEK);
		System.out.println(weekday);
	}
	
	@Test
	public void getTodayWithYMDHMS(){
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String today=df.format(d);
		System.out.println(today);
	}
	
}
