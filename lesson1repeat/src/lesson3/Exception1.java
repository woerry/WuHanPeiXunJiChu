package lesson3;

import org.junit.Test;

public class Exception1 {
	//异常两种应对方式
	@Test
   public void exception1(){
	   int a=0;
	   //1.捕获异常
	   try{//
		   double b;
		   if(a==0){
			   System.out.println("除数不能为0");
		   }else{
			   b =3/a;
		   }
		 
	   }catch(Exception e){
		   e.printStackTrace();
	   }
	  
	   
   }
	
	//2.抛出异常。本身不捕获异常，本着谁调用谁负责，一层一层往上抛
	@Test
	   public void exception2() throws Exception{
		   int a=0;
		  			   double b=3/a;
		  
		  	   }
}
