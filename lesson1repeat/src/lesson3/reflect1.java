package lesson3;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.junit.Test;

public class reflect1 {
	@Test
  public void re1() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
	  testclass tc=new testclass();
	  Class tcc=tc.getClass();
//	  System.out.println(tcc);//全路径
	  Class tcc1=Class.forName("lesson3.testclass");//通过类名来查找类
	  //jdbc加载mysql驱动。
//	  System.out.println(tcc1);//全路径
	  Field[] f= tcc1.getDeclaredFields();//获取testclass的全部字段(成员变量)
	  for(int i=0;i<f.length;i++){
		  System.out.println("testclass类的第"+i+"个字段："+f[i].getName());
		  
	  }
	  testclass tc2= ( testclass)tcc1.newInstance();//实例化方式
	 System.out.println(tc2);
	 Method[] m= tcc1.getDeclaredMethods();
	 for(int i=0;i<m.length;i++){
		  System.out.println("testclass类的第"+i+"个方法："+m[i].getName());
		  
	  }
  }
}
