package lesson3;

public class DrawThread extends Thread {
  //模拟用户账户
	private Account account;
	//当前取钱线程所希望取的钱数
	private double drawAmount;
	
	public DrawThread(){
		
	}
	
	public DrawThread(String name,Account account,double drawAmount){
		super(name);
		this.account=account;
		this.drawAmount=drawAmount;
	}
	
	public  void run(){
		//对 余额进行线程安全保护，加一个锁。
		synchronized (account) {
			//对account进行监控。
			//如果有线程正在调用这个方法块时，那么就会加锁，不准许其他线程调用本方法块，直到方法块运行完，才释放
			//余额大于取钱数目
			if(account.getBalance()>=drawAmount){
				System.out.println(this.getName()+"取钱成功！吐出钞票："+drawAmount);
				//修改余额
				account.setBalance(account.getBalance()-drawAmount);
				System.out.println("余额为："+account.getBalance());
			}else{
				System.out.println(this.getName()+"取钱失败！余额不足");
			}
		}
		
	}
	
	
}
