package lesson3;

/**
 * 
 * @author Administrator
 *
 * 银行取钱问题（著名的题目）
 * 1.用户输入账号，密码，系统判断用户的账号，密码是否匹配
 * 2.用户输入取款金额
 * 3.系统判断账户余额是否大于取款金额
 * 4.如果余额大于取款金额，则取款成功；如果余额小于取款金额，则取款失败；
 *
 */
public class Account {
	//账号
	private String accountNo;
	//余额
	private double balance;
	
	
	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Account(){
		
	}
	
public Account(String accountNo, double balance){
		this.accountNo=accountNo;
		this.balance=balance;
	}

	public boolean equals(Object obj){
		if(this==obj){
			
		
			return true;//return 如果运行了return，那么不管下面写了啥，直接跳出本方法。
		}
		if(obj!=null&&obj.getClass()==Account.class){//传人的对象就是Account类的对象
			Account target=(Account)obj;
			return target.getAccountNo().equals(accountNo);//存在的基础在于必须用Account(String accountNo, double balance)方法来构造对象。
		}
		return false;
		
	}
  
}
