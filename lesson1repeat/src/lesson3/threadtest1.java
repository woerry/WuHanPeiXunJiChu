package lesson3;

public class threadtest1 extends Thread {
  //继承Thread类之后，需要重写run()方法。需要线程执行的方法体就放在run()中。
	public void run(){
		for(int i=0;i<50;i++){
			//当使用继承线程类的方法创建时，直接使用this.getName().
			//this是在类中，表示本类。
			System.out.println(this.getName()+" "+i);
		}
	}
}
