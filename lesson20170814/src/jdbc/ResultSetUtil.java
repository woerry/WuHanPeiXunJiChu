package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ResultSetUtil {
	private ResultSet rs=null;
	
	/**
	 * 获取记录集
	 * @param sm
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public ResultSet getResult(Statement sm,String sql) throws SQLException{
		rs=sm.executeQuery(sql);
		return rs;
		
	}
	
	
	public ResultSet getResult(PreparedStatement psm) throws SQLException{
		rs=psm.executeQuery();
		return rs;
		
	}
	
	/**
	 * 关闭记录集
	 * @throws SQLException
	 */
	public void close() throws SQLException{
		rs.close();
	}

}
