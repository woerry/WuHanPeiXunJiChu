package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
  private Connection conn=null;
  
  /**
   *  获取数据库连接
   * @return
 * @throws ClassNotFoundException 
 * @throws SQLException 
   */
  public Connection getConn() throws ClassNotFoundException, SQLException{
	  Class.forName("com.mysql.jdbc.Driver");//利于反射获取mysql驱动（全路径）
	  //&nbsp;  表示空格  xml,html &amp;表示;
	  String url="jdbc:mysql://127.0.0.1:3306/whjava?useUnicode=true&amp;characterEncoding=utf-8";
	  String user="root";
	  String pwd="x5";
	  conn=DriverManager.getConnection(url,user,pwd);
	return conn;
	  
  }
  
  /**
   * 释放数据库连接
   * @throws SQLException
   */
  public void close() throws SQLException{
	  conn.close();
  }
  
  
}
