package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class test {
  
	@Test
	public void testsql() throws ClassNotFoundException, SQLException{
		ConnectionUtil connutil=new ConnectionUtil();
		Connection conn=connutil.getConn();
		StatementUtil su=new StatementUtil();
		Statement sm=su.getStatement(conn);
		ResultSetUtil ru=new ResultSetUtil();
		String sql=" select * from area ";
		ResultSet rs=ru.getResult(sm, sql);
		while(rs.next()){
			System.out.println(rs.getInt(1)+"-"+rs.getString(2)+"-"+rs.getString(3));
		}
		conn.close();
		sm.close();
		rs.close();//有返回值时，比如select时才有结果集，才需要关闭
	}
	
	@Test
	public void testsql1() throws ClassNotFoundException, SQLException{
		ConnectionUtil connutil=new ConnectionUtil();
		Connection conn=connutil.getConn();
		String sql=" select * from area ";
		//String的拼接，并不是单纯的拼接，从内存角度来说，它是新申请地址，然后将原地址指向的内容
		//和新加内容连接在一起
		//sql
		PreparedStatementUtil su=new PreparedStatementUtil();
		PreparedStatement sm=su.getPreparedStatement(conn, sql);
		ResultSetUtil ru=new ResultSetUtil();
	
		ResultSet rs=ru.getResult(sm);
		while(rs.next()){
			System.out.println(rs.getInt(1)+"-"+rs.getString(2)+"-"+rs.getString(3));
		}
		conn.close();
		sm.close();
		rs.close();//有返回值时，比如select时才有结果集，才需要关闭
	}
	
	@Test
	public void str(){
		String sql=" select * from area ";
		String sql1=sql+" a";
		if(sql==sql1){
			System.out.println(1);
		}else{
			System.out.println(2);
		}
		StringBuffer sb=new StringBuffer();
		sb.append(" select * from area");
		sb.append(" a");
		String sql3=sb.toString();
		//sql拼接换行
	}
	
}
