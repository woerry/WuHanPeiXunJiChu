package jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementUtil {
  private Statement sm=null;
  
  /**
   * 获取statement
   * @return
 * @throws SQLException 
   */
  public Statement getStatement(Connection conn) throws SQLException{
	 sm= conn.createStatement();
	return sm;
	  
  }
	
  /**
   * 释放statement 
   * @throws SQLException
   */
  public void close() throws SQLException{
	  sm.close();
  }
	
}
