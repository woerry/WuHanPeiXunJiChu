package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementUtil {
  PreparedStatement psm=null;
  
  /**
   * 获取预编译报表
   * @param conn
   * @param sql
   * @return
   * @throws SQLException
   */
   public   PreparedStatement getPreparedStatement(Connection conn,String sql) throws SQLException{
	   psm=conn.prepareStatement(sql);
	return psm;
	   
   }
   
   public void close() throws SQLException{
	   psm.close();
   }
}
