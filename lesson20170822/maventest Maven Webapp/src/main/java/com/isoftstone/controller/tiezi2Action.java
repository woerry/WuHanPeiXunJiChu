package com.isoftstone.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.isoftstone.dao.demo;

public class tiezi2Action extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public tiezi2Action() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=utf-8");
		request.setCharacterEncoding("UTF-8");
		String title=request.getParameter("title");
		String str = new String(title.getBytes("ISO-8859-1"),"utf-8");
		System.out.println("title="+str);
		List<Map<String,Object>> l=new demo().getDemo();
		int now=0;
		for(int i=0;i<l.size();i++){
			if(l.get(i).get("title").equals(title)){
				now=i;
				break;
			}
		}

		Map<String,Object> m=l.get(now);
		
		System.out.println(JSON.toJSON(m));
		
		PrintWriter out = response.getWriter();
	
//		String jsonStr = "{\"name\":\"fly\",\"type\":\"虫子\"}";
//		
//		out.write(jsonStr);
		out.print(JSON.toJSON(m));
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
