package com.isoftstone.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.isoftstone.dao.demo;
public class fenyeAction extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public fenyeAction() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Map<String,Object>> l=new demo().getDemo();
		      String c=request.getParameter("current");
		      System.out.println("action.current="+c);
		  	Integer current=0;
		      if((!c.equals(""))&&c!=null){
		    	  current=Integer.valueOf(c) ;
		      }
		      int count=l.size();//记录的总数
		      final int pageSize=2;//每页显示多少条记录
//		      int current=1;//默认显示第一页
		      int pageCount=0;//总页数
		      if(count%pageSize==0){//如果能整除
		       pageCount=count/pageSize;
		      }else{
		       pageCount=(int)(count/pageSize)+1;
		      }
		      List<Map<String,Object>> l1=new ArrayList();
		      for(int i=(current-1)*pageSize;i<current*pageSize;i++){
		    	  l1.add(l.get(i));
		    	  		      }
		      
		      System.out.println("action.current="+c);
		      System.out.println("action.pageCount="+pageCount);
		      System.out.println("action.pageSize="+pageSize);
		      System.out.println("action.count="+count);
		      request.setAttribute("pageCount",  pageCount);
		request.setAttribute("current", current);
		   request.setAttribute("pageSize",  pageSize);
			request.setAttribute("count", count);
			request.setAttribute("data", l1);
		request.getRequestDispatcher("./table.jsp").forward(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	this.doPost(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
